require 'test_helper'

class SupervisorTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  
  def setup
     @Supervisor = Supervisor.new(svname: "Mtokkzk",
                                  password: "foobar", password_confirmation: "foobar")
     
  end
  
  test "password should be no present (nonblank) "do
    @Supervisor.password = @Supervisor.password_confirmation = " " * 6
    assert_not @Supervisor.valid?
  end
  
  test "password should have a minimun length " do
    @Supervisor.password = @Supervisor.password_confirmation = "a" * 5
    assert_not @Supervisor.valid?
  end
  
end
