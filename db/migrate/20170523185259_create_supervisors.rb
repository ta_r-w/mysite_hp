class CreateSupervisors < ActiveRecord::Migration
  def change
    create_table :supervisors do |t|
      t.string :svname
      t.string :password_digest

      t.timestamps null: false
    end
  end
end
