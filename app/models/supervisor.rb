class Supervisor < ActiveRecord::Base
    
    validates :svname, presence: true
    
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
    
end
