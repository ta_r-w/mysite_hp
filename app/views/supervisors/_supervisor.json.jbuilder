json.extract! supervisor, :id, :svname, :password_digest, :created_at, :updated_at
json.url supervisor_url(supervisor, format: :json)